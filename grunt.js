module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        /*concat: {
            dist: {
                src: ['js*//*.js'],
                dest: 'dist/js/springcloud.js'
            }
        },*/
        less: {
            production: {
                files: {
                    "sc_boilerplate/css/springcloud.css": "less/index.less"
                }
            }
        }
    });

    // Load tasks from "grunt-sample" grunt plugin installed via Npm.
    grunt.loadNpmTasks('grunt-contrib-less');

    // Default task.
    grunt.registerTask('default', 'less');

};