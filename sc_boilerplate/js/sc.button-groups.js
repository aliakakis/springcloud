(function($) {
    $.fn.buttonGroups = function() {
        var btnGroup = $(this);

        function metrics(el) {
            var btn = el.find("button"),
                arr = [],
                largest;

            for (var k = 0; k < btn.length; k++) {
                arr.push($(btn[k]).outerWidth());
            }
            largest = Math.max.apply(Math, arr);
            for (var j = 0; j < btn.length; j++) {
                $(btn[j]).css("min-width", largest);
                $(btn[j]).parent().css("width", largest * btn.length);
            }
        }

        for (var i = 0; i < btnGroup.length; i++) {
            if ($(btnGroup[i]).attr('data-toggle') == "buttons-radio") {
                metrics($(btnGroup[i]));
            }
        }
        return $(this);
    };
})(jQuery);