$(function() {
    $(window).load(function() {

        init();

        //Please avoid changing current code in init.
        function init() {
            var count = 0,
                bgArray = ["bg001", "bg002", "bg003", "bg004",
                    "bg006", "bg007", "bg008",
                    "bg009", "bg010", "bg011"],
                colorArray = ["#292929", "#1a3f61", "#303847", "#5e613c",
                    "#30496e", "#3d4d7b", "#251b20",
                    "#5e5f55", "#4e4854", "#475c70"],
                randomNum = Math.floor(Math.random() * bgArray.length),
                bgImage = "img/"+bgArray[randomNum]+".jpg";

            //Set random background images, background colors and text colors.
            $("#bg > img").attr("src", bgImage);
            $(".bg-color").css({
                "background-color": colorArray[randomNum]
            });
            $(".large-text").css({
                "color": colorArray[randomNum]
            });

            //Set animation transitions for elements.
            $(".input-error-message > div:first-child, .error").hide();
            $(window).keyup(function(e) {
                if (e.keyCode == 32 && count == 0) {
                    count = 1;
                    $(".input-error-message > div:first-child, .error").show();
                }
                else if (e.keyCode == 32 && count == 1) {
                    count = 0;
                    $(".input-error-message > div:first-child, .error").hide();
                }
            });

            //Dropdown component.
            $(".select2-dropdown").select2({
                search: true,
                tooltip: true
            });

            //Handle forgot password
            $("#forgotPass").on("click", function() {
                $(".login-form").hide();
                $(".password-form").show();
            });

            $("#submitPass").on("click", function() {
                $(".login-form").show();
                $(".password-form").hide();
            });
        }

    });
});
