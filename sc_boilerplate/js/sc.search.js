(function($) {
    $.fn.search = function(options) {
        var inputText = $(this);

        for (var i = 0; i < inputText.length; i++) {
            $(inputText[i]).next().hide();

            draw($(inputText[i]).next().next(), options.search);

            $(inputText[i]).on("input", function() {
                if ($(this).val() != "") {
                    $(this).next().show();
                    var rv = -1,
                        ua = navigator.userAgent,
                        re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");

                    if (re.exec(ua) != null) {
                        if (re.exec(ua) == "MSIE 9.0,9.0") {
                            $(this).next().hide();
                        }
                        else {
                            $(this).next().css({
                                "left": $(this).width() + 15,
                                "top": $(this).outerHeight() / 2 - $(this).next().outerHeight() / 2
                            });
                        }
                    }
                    else {
                        $(this).next().css({
                            "left": $(this).width() + 15,
                            "top": $(this).outerHeight() / 2 - $(this).next().outerHeight() / 2
                        });
                    }
                }
                else {
                    $(this).next().hide();
                }
                $(this).next().on("click", function() {
                    $(this).prev().val("");
                    $(this).hide();
                });
            });
            $(inputText[i]).next().next().css({
                "margin-left": "-1px",
                "margin-top": "-1px"
            });

            if ($(inputText[i]).val != "") {
                $(inputText[i]).trigger("input");
            }
        }

        function draw(el, state) {
            if (state == true) {
                el.show();
            }
            else {
                el.hide();
            }
        }

        return $(this);
    };
})(jQuery);