(function () {
    // Private function
    function getColumnsForScaffolding(data) {
        if ((typeof data.length !== 'number') || data.length === 0) {
            return [];
        }
        var columns = [];
        for (var propertyName in data[0]) {
            columns.push({ headerText: propertyName, rowText: propertyName });
        }
        return columns;
    }

    ko.simpleGrid = {
        // Defines a view model class you can use to populate a grid
        viewModel: function (configuration) {
            this.data = configuration.data;
            this.rows = configuration.rows;
            this.currentPageIndex = ko.observable(0);
            this.pageSize = ko.observable(5);
            this.selectedRows = ko.observable('5');

            // Refresh the paging and trigger the first page for the arrow to appear
            this.onSelectedRows = function() {
                this.pageSize(parseInt(this.selectedRows()));
                var page = $(".pagination ul li a");
                $(page[1]).trigger("click");
            };

            // If you don't specify columns configuration, we'll use scaffolding
            this.columns = configuration.columns || getColumnsForScaffolding(ko.utils.unwrapObservable(this.data));

            this.itemsOnCurrentPage = ko.computed(function () {
                var startIndex = this.pageSize() * this.currentPageIndex();
                return this.data.slice(startIndex, startIndex + this.pageSize());
            }, this);

            this.maxPageIndex = ko.computed(function () {
                return Math.ceil(ko.utils.unwrapObservable(this.data).length / this.pageSize()) - 1;
            }, this);
        }
    };

    // Templates used to render the grid
    var templateEngine = new ko.nativeTemplateEngine();

    templateEngine.addTemplate = function(templateName, templateMarkup) {
        document.write("<script type='text/html' id='" + templateName + "'>" + templateMarkup + "<" + "/script>");
    };

    templateEngine.addTemplate("ko_simpleGrid_grid", "\
                    <table class=\"ko-grid\" cellspacing=\"0\">\
                        <thead>\
                            <tr data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText, \
                                               css: { 'sort-alpha-beta': sortBy == 'alphabetically', \
                                               'sort-numeric': sortBy == 'numerically', \
                                               'left': align == 'left', \
                                               'right': align == 'right', \
                                               'center': align == 'center' }, \
                                               attr: {'data-sort-name': sortName }\">\
                               </th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                           <tr class=\"row-data\" data-bind=\"foreach: $parent.columns\">\
                               <td data-bind=\"css: { 'left': align == 'left', 'right': align == 'right', 'center': align == 'center' }, attr: {'data-name': typeof rowText == 'function' ? rowText($parent) : $parent[rowText] }\">\
                                   <span data-bind=\"text: typeof rowText == 'function' ? rowText($parent) : $parent[rowText]\"></span>\
                                   <button data-bind=\"visible: button == 'view-app', css: { 'btn-view-app': button == 'view-app' }\" type=\"button\" class=\"btn btn-info\">VIEW APP</button>\
                                   <img src=\"../img/b_arrow_c_idle.png\" data-bind=\"visible: button == 'edit-button' , css: { 'btn-edit': button == 'edit-button' }\"/>\
                                   <div data-bind=\"visible: false, css: { 'edit': button == 'edit-button' }\">\
                                        <div class=\"edit-header\">\
                                            <img class=\"popup-arrow\" src=\"../img/arrow_pop_left.png\"/>\
                                            <button type=\"button\" class=\"btn\">Edit</button>\
                                            <button type=\"button\" class=\"btn btn-thumb\">\
                                                <img src=\"../img/trash_a.png\"/>\
                                            </button>\
                                        </div>\
                                        <div class=\"edit-body\">\
                                            <div>\
                                                <span>Clients</span>\
                                                <span>(15)</span>\
                                                <button type=\"button\" class=\"btn btn-thumb client\">\
                                                    <img src=\"../img/button_add.png\"/>\
                                                </button>\
                                            </div>\
                                            <div>\
                                                <span>Advertisers</span>\
                                                <span>(35)</span>\
                                                <button type=\"button\" class=\"btn btn-thumb advert\">\
                                                    <img src=\"../img/button_add.png\"/>\
                                                </button>\
                                            </div>\
                                            <div>\
                                                <span>Users</span>\
                                                <span>(20)</span>\
                                                <button type=\"button\" class=\"btn btn-thumb user\">\
                                                    <img src=\"../img/button_add.png\"/>\
                                                </button>\
                                            </div>\
                                        </div>\
                                   </div>\
                               </td>\
                            </tr>\
                        </tbody>\
                    </table>");

    templateEngine.addTemplate("ko_simpleGrid_pageLinks", "\
                    <div class=\"ko-grid-pageLinks\">\
                        <div id=\"pagination1\" class=\"sc-pagination\">\
                            <div>Display</div>\
                            <select class=\"select2-dropdown\" style=\"width: 80px;\" data-bind=\"options: rows, value: selectedRows, event: {change: onSelectedRows}\"></select>\
                            <div class=\"pagination\">\
                                <ul>\
                                    <li><a href=\"#\">Prev</a></li>\
                                    <li>\
                                        <!-- ko foreach: ko.utils.range(0, maxPageIndex) -->\
                                        <a href=\"#\" data-bind=\"text: $data + 1, click: function() { $root.currentPageIndex($data) }\"></a>\
                                        <!-- /ko -->\
                                    </li>\
                                    <li><a href=\"#\">Next</a></li>\
                                </ul>\
                                </div>\
                            <img src=\"../img/paging_arrow.png\"/>\
                        </div>\
                    </div>");

    // The "simpleGrid" binding
    ko.bindingHandlers.simpleGrid = {
        init: function() {
            return { 'controlsDescendantBindings': true };
        },
        // This method is called to initialize the node, and will also be called again if you change what the grid is bound to
        update: function (element, viewModelAccessor, allBindingsAccessor) {
            var viewModel = viewModelAccessor(), allBindings = allBindingsAccessor();

            // Empty the element
            while(element.firstChild)
                ko.removeNode(element.firstChild);

            // Allow the default templates to be overridden
            var gridTemplateName      = allBindings.simpleGridTemplate || "ko_simpleGrid_grid",
                pageLinksTemplateName = allBindings.simpleGridPagerTemplate || "ko_simpleGrid_pageLinks";

            // Render the main grid
            var gridContainer = element.appendChild(document.createElement("DIV"));
            ko.renderTemplate(gridTemplateName, viewModel, { templateEngine: templateEngine }, gridContainer, "replaceNode");

            // Render the page links
            var pageLinksContainer = element.appendChild(document.createElement("DIV"));
            ko.renderTemplate(pageLinksTemplateName, viewModel, { templateEngine: templateEngine }, pageLinksContainer, "replaceNode");
        }
    };
})();