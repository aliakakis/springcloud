$(function() {
    $(window).load(function() {
        jQuery("#list11").jqGrid({
            url:'http://trirand.com/blog/jqgrid/server.php?q=1',
            datatype: "xml",
            height: 200,
            colNames:['Inv No','Date', 'Client', 'Amount','Tax','Total','Notes'],
            colModel:[
                {name:'id',index:'id', width:140, align:"center"},
                {name:'invdate',index:'invdate', width:140, align:"center"},
                {name:'name',index:'name', width:140, align:"center"},
                {name:'amount',index:'amount', width:140, align:"center"},
                {name:'tax',index:'tax', width:140, align:"center"},
                {name:'total',index:'total', width:140, align:"center"},
                {name:'note',index:'note', width:140, sortable:false, align:"center"}
            ],
            rowNum:10,
            rowList:[10,20,30],
            pager: '#pager11',
            scroll: true,
            sortname: 'id',
            viewrecords: true,
            sortorder: "desc",
            multiselect: false,
            subGrid : true,
            subGridUrl: 'http://trirand.com/blog/jqgrid/subgrid.php?q=2',
            subGridModel: [{ name  : ['No','Item','Qty','Unit','Line Total'],
                width : [140,140,140,140,140] }
            ]
        });
        jQuery("#list11").jqGrid('navGrid','#pager11',{add:false,edit:false,del:false});
    });
});