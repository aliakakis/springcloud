$(function() {
    //Components init code
    $(window).load(function() {
        $(".sc-menu").menu();
        $("#search").search({
            search: true /* Show hide search button */
        });
        $(".select2-dropdown").select2({
            search: false,
            tooltip: true
        });
        $(".btn-group").buttonGroups();
        $(".sc-pagination").pagination();
    });
});