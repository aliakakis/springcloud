(function($) {
    $.fn.menu = function() {
        var heads = $(this).find(".sc-menu-heading"),
            bodies = $(this).find(".sc-menu-body");

        for (var i = 0; i < heads.length; i++) {
            $(heads[i]).on("mouseover", function() {
                for (var k = 0; k < heads.length; k++) {
                    $(heads[k]).removeClass("hover");
                    $(heads[k]).next().hide();
                }
                if ($(this).hasClass("arrow-down")) {
                    $(this).addClass("hover");
                    $(this).next().show();
                }
            });
            $(heads[i]).on("click", function() {
                $(this).parent().parent().find("img").css({
                    left: ($(this).offset().left + ($(this).outerWidth() / 2)) - 11,
                    top: ($(this).outerHeight() - 13) + $(this).offset().top
                });
            });
        }
        $(heads[0]).trigger("click");

        if ($(this).find(".sc-menu-inner").length != 0) {
            for (var j = 0; j < bodies.length; j++) {
                $(bodies[j]).on("click", function() {
                    $(this).prev().removeClass("hover");
                    $(this).hide();
                });
                $(bodies[j]).mouseleave(function() {
                    $(this).prev().removeClass("hover");
                    $(this).hide();
                });
            }
        }

        return $(this);
    };
})(jQuery);