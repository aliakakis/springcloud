(function($) {
    $.fn.switcher = function(options) {
        var switches = $(this),
            metric;

        function metrics(el) {
            var arr = [],
                largest;
            arr.push(el.find(".switch div:nth-child(1)").outerWidth());
            arr.push(el.find(".switch div:nth-child(3)").outerWidth());
            largest = Math.max.apply(Math, arr);

            el.outerWidth(largest * 2);
            el.find(".switch div:nth-child(1)").outerWidth(largest);
            el.find(".switch div:nth-child(2)").outerWidth(largest);
            el.find(".switch div:nth-child(3)").outerWidth(largest);
            el.find(".switch").outerWidth(largest * 3);
            el.find(".switch").css("margin-left", "0px");

            return largest;
        }

        for (var i = 0; i < switches.length; i++) {
            metric = metrics($(switches[i]));
            if ( $(switches[i]).attr('data-state') == "false" ) {
                $(switches[i]).find(".switch").css("margin-left", "-"+metric+"px");
                $(switches[i]).toggle(
                    function(){
                        $(this).find(".switch").animate({"margin-left": "0px"}, 300, 'linear', function() {
                            options.onYesEvent();
                        });
                    },
                    function(){
                        var num = $(this).find(".switch div:nth-child(1)").outerWidth();
                        $(this).find(".switch").animate({"margin-left": "-"+num+"px"}, 300, 'linear', function() {
                            options.onNoEvent();
                        });
                    }
                );
            }
            else {
                $(switches[i]).find(".switch").css("margin-left", "0px");
                $(switches[i]).toggle(
                    function(){
                        var num = $(this).find(".switch div:nth-child(1)").outerWidth();
                        $(this).find(".switch").animate({"margin-left": "-"+num+"px"}, 300, 'linear', function() {
                            options.onNoEvent();
                        });
                    },
                    function(){
                        $(this).find(".switch").animate({"margin-left": "0px"}, 300, 'linear', function() {
                            options.onYesEvent();
                        });
                    }
                );
            }
        }
        return $(this);
    };
})(jQuery);