(function($) {
    $.fn.pagination = function() {
        var pagination = $(this),
            el = $(this).find(".pagination ul li a"),
            counter = 0,
            pages = el.length - 3,
            store;

        for (var i = 0; i < pagination.length; i++) {
            if ($(pagination[i]).hasClass("white")) {
                $(pagination[i]).find("img").attr("src", "../img/paging_arrow_white.png");
            }
        }

        for (var i = 1; i < el.length - 1; i++) {
            $(el[i]).attr("data-counter", i);
            $(el[i]).on("click", function() {
                counter = parseInt($(this).attr("data-counter")) - 1;
                $(pagination[0]).find("img").css({
                    left: ($(this).offset().left + ($(this).outerWidth() / 2)) - 10,
                    top: $(this).offset().top - 9
                });
                store = $(this);
            });
        }
        $(el[1]).trigger("click");

        $(window).on("resize", function(){
            $(store).trigger("click");
        });

        // Previous
        $(el[0]).on("click", function() {
            if (counter != 0) {
                $(el[counter]).trigger("click");
            }
        });
        // Next
        $(el[el.length - 1]).on("click", function() {
            if (counter < pages) {
                counter++;
                $(el[counter + 1]).trigger("click");
            }
        });

        return $(this);
    };
})(jQuery);