(function($) {
    $.fn.datagrid = function() {
        var heads = $(this).find(".sc-datagrid-heading > div"),
            rows = $(this).find(".sc-datagrid-row"),
            rowDivs,
            children,
            childrenRows,
            filterRows,
            hideRows;
        hideChildrenRows();

        for (var i = 0; i < heads.length; i++) {
            if ($(heads[i]).hasClass("sorting")) {
                $(heads[i]).toggle(
                    function(){
                        $(this).css("background-image", 'url("../img/arrow-blue-up.png")');
                    },
                    function(){
                        $(this).css("background-image", 'url("../img/arrow-blue-down.png")');
                    }
                );
            }
        }

        $(".row-toggle").toggle(
            function(){
                //Expand
                $(this).css("background-image", 'url("../img/arrow-greydown_down.png")');
                children = $(this).parent().parent().parent().children();
                if (children.length > 1) {
                    filterRows = children.slice(1); //We do not need the first row since it is the parent row
                    for (var i = 0; i < filterRows.length; i++) {
                        $(filterRows[i]).find("div:first-child > div:first-child > div").css({
                            "background-position": ($(filterRows[i]).parents().length - 3) * 10 + "%",
                            "text-indent": (($(filterRows[i]).parents().length - 3) * 13) + 13
                        });
                        $(filterRows[i]).slideDown(0);
                    }
                }
            },
            function(){
                //Retract
                $(this).css("background-image", 'url("../img/arrow-greydown_right.png")');
                children = $(this).parent().parent().parent().children();
                if (children.length > 1) {
                    filterRows = children.slice(1); //We do not need the first row since it is the parent row
                    for (var i = 0; i < filterRows.length; i++) {
                        $(filterRows[i]).slideUp(0);
                    }
                }
            }
        );

        for (var j = 0; j < rows.length; j++) {
            $(rows[j]).on("mouseover", function() {
                $(this).addClass("hover");
            });
            $(rows[j]).on("mouseout", function() {
                $(this).removeClass("hover");
            });
            if ($(rows[j]).hasClass("disable-row")) {
                $(rows[j]).off();
            }
        }

        function hideChildrenRows() {
            rowDivs = $('div[data-row="child"]');
            for (var i = 0; i < rowDivs.length; i++) {
                $(rowDivs[i]).slideUp(1000, function() {

                });
            }
        }

        return $(this);
    };
})(jQuery);