(function($) {
    $.fn.calendar = function() {
        var dates = [];
        $(this).datepicker({
            hideOnSelect: false,
            numberOfMonths: 2,
            showButtonPanel: true,
            closeText: "Apply",
            beforeShow: function(input, inst) {

            },
            onSelect: function(dateText, inst) {
                dates.push(dateText);
                if (dates.length > 2) {
                    dates = [];
                    dates.push(dateText);
                }
            },
            onClose: function(dateText, inst) {
                switch (dates.length) {
                    case 0:
                        $(this).val("");
                        break;
                    case 1:
                        dates.push(dates[0]);
                        $(this).val(dates[0]);
                        break;
                    default:
                        $(this).val(dates[0] +" - "+ dates[1]);
                        break;
                }
            }
        });
        return $(this);
    };
})(jQuery);