function PagedGridModel(items, rowsPerPage, columns) {
    this.items = ko.observableArray(items);
    this.rows =  ko.observableArray(rowsPerPage);
    this.columns = columns;

    this.addItem = function() {
        this.items.push({ name: "New item", sales: 0, price: 100 });
    };

    this.sortByNameAtoZ = function(rowName) {
        this.items.sort(function(a, b) {
            return a[rowName] < b[rowName] ? -1 : 1;
        });
    };

    this.sortByNameZtoA = function(rowName) {
        this.items.sort(function(a, b) {
            return b[rowName] < a[rowName] ? -1 : 1;
        });
    };

    this.sortByNumbersAscending = function(rowName) {
        this.items.sort(function(a, b) {
            return a[rowName] - b[rowName];
        });
    };

    this.sortByNumbersDescending = function(rowName) {
        this.items.sort(function(a, b) {
            return b[rowName] - a[rowName];
        });
    };

    this.jumpToFirstPage = function() {
        this.gridViewModel.currentPageIndex(0);
    };

    this.deleteCampaign = function(name) {
        var campaignName;
        for (var i = 0, len = items.length; i < len; i++) {
            if (items[i].name === name) {
                campaignName = items[i];
            }
        }
        items.splice(items.indexOf(campaignName), 1);
    };

    // Initialize components
    this.enableGrid = function() {
        var self = this;
        window.setTimeout(function() {
            $(".select2-dropdown").select2({
                search: false
            });
            $(".sc-pagination").pagination();
            $(".sort-alpha-beta").toggle(
                function(){
                    $(this).css("background-image", 'url("../img/arrow-blue-up.png")');
                    self.sortByNameZtoA($(this).attr("data-sort-name"));
                    self.enableEditProperties();
                },
                function(){
                    $(this).css("background-image", 'url("../img/arrow-blue-down.png")');
                    self.sortByNameAtoZ($(this).attr("data-sort-name"));
                    self.enableEditProperties();
                }
            );
            $(".sort-numeric").toggle(
                function(){
                    $(this).css("background-image", 'url("../img/arrow-blue-up.png")');
                    self.sortByNumbersAscending($(this).attr("data-sort-name"));
                    self.enableEditProperties();
                },
                function(){
                    $(this).css("background-image", 'url("../img/arrow-blue-down.png")');
                    self.sortByNumbersDescending($(this).attr("data-sort-name"));
                    self.enableEditProperties();
                }
            );

            // Call enable properties each time a new page loads
            $(".pagination a").on("click", function() {
                self.enableEditProperties();
            });

            self.sortByNameAtoZ("name");
            self.enableEditProperties();
        }, 250);
    };

    // Hover event for row and edit button when
    // the page loads for the first time.
    this.enableEditProperties = function() {
        var self = this;
        window.setTimeout(function() {
            $(".row-data").off("mouseout").on("mouseover", function() {
                $(this).find(".btn-edit").show();
                $(this).find(".btn-view-app").show();
            });
            $(".row-data").off("mouseout").on("mouseout", function() {
                $(this).find(".btn-edit").hide();
                $(this).find(".btn-view-app").hide();
            });
            $(".btn-view-app").off("click").on("click", function() {
                console.log($(this).parents("td").attr("data-name"));
            });
            $(".btn-edit").off("click").on("click", function() {
                $(this).parents("tr.row-data").off("mouseout");
                var toggle = $(".btn-edit");
                for (var i = 0; i < toggle.length; i++) {
                    $(toggle[i]).attr("src","../img/b_arrow_c_idle.png");
                    $(toggle[i]).hide();
                    $(toggle[i]).next().hide();
                }
                $(this).attr("src","../img/b_arrow_c_press.png");

                $("tr.row-data").attr("data-edit", "false");
                $(this).parents("tr.row-data").attr("data-edit", "true");

                $(this).next().css({
                    "left": $(this).parent().find("span:first-child").outerWidth() + 26 + 50,
                    "top": $(this).parent().offset().top + 3
                });
                $(this).next().show();

                // Handle hide on click out of view boundaries
                $(this).next().off("mouseover").on("mouseover", function() {
                    $(window).off("mouseup");
                });
                $(this).next().off("mouseout").on("mouseout", function() {
                    // Hide edit view when click outside boundaries
                    $(window).off("mouseup").on("mouseup", function(e){
                        var btn = $(".btn-edit");
                        if (btn.has(e.target).length === 0) {
                            btn.attr("src","../img/b_arrow_c_idle.png");
                            btn.hide();
                            btn.next().hide();
                        }
                        $(".row-data").off("mouseout").on("mouseout", function() {
                            $(this).find(".btn-edit").hide();
                        });
                    });
                });

                // Edit view
                var storeRow, campaignName;
                // Edit button found on the edit view menu
                $(this).next().find(".edit-header > img:first-child + button").off("click").on("click", function() {
                    console.log($(this).parents("td").attr("data-name"));
                });
                // Delete button found on the edit view menu
                $(this).next().find(".edit-header > button:last-child").off("click").on("click", function() {

                });
                // Clients button
                $(this).next().find(".edit-body > div:first-child > .client").off("click").on("click", function() {

                });
                // Advertisers button
                $(this).next().find(".edit-body > div:first-child + div > .advert").off("click").on("click", function() {

                });
                // Users button
                $(this).next().find(".edit-body > div:last-child > .user").off("click").on("click", function() {

                });
            });
        }, 500);
    };

    this.gridViewModel = new ko.simpleGrid.viewModel({
        data: this.items,
        columns: this.columns,
        rows: this.rows
    });
};

