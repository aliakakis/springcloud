(function($) {
    $.fn.accordion = function(options) {
        var body = $(this).find(".sc-accordion-body"),
            inners = $(this).find(".sc-accordion-inner");

        for (var i = 0; i < body.length; i++) {
            $(body[i]).hide();
            if ($(body[i]).hasClass("selected-body")) {
                $(body[i]).prev().find("div").addClass(options.color);
                $(body[i]).prev().find("img").show();
                $(body[i]).show();
            }
        }
        for (var i = 0; i < inners.length; i++) {
            if ($(inners[i]).hasClass("selected-inner")) {
                $(inners[i]).parent().find(".sc-accordion-inner").find("div").removeClass(options.color);
                $(inners[i]).parent().find(".sc-accordion-inner").find("img").hide();
                $(inners[i]).find("div").addClass(options.color);
                $(inners[i]).find("img").show();
            }
        }

        $(this).find(".sc-accordion-heading").each(function (index, el) {
            arrowColor(options.color, $(this).parent());
            $(this).on("click", function() {
                var inner = $(this).parent().find(".sc-accordion-body");
                if (inner.css("display") == "block") {
                    $(this).next().find(".sc-accordion-inner").find("div").removeClass(options.color);
                    $(this).next().find(".sc-accordion-inner").find("img").hide();
                    $(this).find("div").removeClass(options.color);
                    $(this).find("img").hide();
                    inner.slideUp("slow");
                }
                else {
                    $(this).next().find(".sc-accordion-inner").find("div").removeClass(options.color);
                    $(this).next().find(".sc-accordion-inner").find("img").hide();
                    body.prev().find("div").removeClass(options.color);
                    body.prev().find("img").hide();
                    $(this).find("div").addClass(options.color);
                    $(this).find("img").show();
                    body.slideUp();
                    inner.slideDown("slow");
                }
            });
            $(this).next().find(".sc-accordion-inner").on("click", function(){
                $(this).parent().find(".sc-accordion-inner").find("div").removeClass(options.color);
                $(this).parent().find(".sc-accordion-inner").find("img").hide();
                $(this).find("div").addClass(options.color);
                $(this).find("img").show();
            });
        });

        function arrowColor(color, el) {
            switch (color) {
                case "orange-active":
                    el.find("img").attr("src", "../img/arrow_mustrd.png");
                    break;
                case "blue-active":
                    el.find("img").attr("src", "../img/blue_left.png");
                    break;
            }
        }

        return $(this);
    };
})(jQuery);