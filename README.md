## Synopsis

This custom UI/UX library was built in order to be used by the various products at Velti.

## Motivation

This project reflects my work for 10 months. Although the products are in various states of freeze this library should be used by others since it has a lot of useful components.

## Installation

Just place the project in a apache htdocs installation.
The folder "html" has all the components individually while the folder "sc_boilerplate" has everything you need to start on a project.

## License

Currently you can test and use the code without any problem apart from mentioning the creator.